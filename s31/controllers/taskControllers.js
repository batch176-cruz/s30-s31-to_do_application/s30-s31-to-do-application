// Controllers contain the functions and business logic of our Express JS Application

const Task = require("../models/task");

// [CONTROLLERS]

// [SECTION] - CONTROLLERS

// [SECTION] - Create
	module.exports.createTask = (requestBody) => {

		let newTask = new Task({
			name: requestBody.name
		});

		return newTask.save().then((task, error) => {
			if (error) {
				return false
			} else {
				return task
			}
		})
	}

// [SECTION] - Retrieve
	module.exports.getAllTasks = () => {

		return Task.find({}).then(result => {
			return result
		})
	};

// [SECTION] - Update
	// 1. Change status of Task. pending -> completed
	// we will reference the document using it's ID field
	module.exports.taskCompleted = (taskId) => {
		// query/search for the desired task to update
		// the "findById" mongoose method will look for a resource (task) which matches the ID from the URL of the request
		// upon performing this method inside our collection, a new promise will be instantiated, so we need to be able to handle the possible outcome of that promise.
		return Task.findById(taskId).then((found, error) => {
			// describe how were going to handle the outcome of the promise using a selection control structure.
			// error -> rejected state of the promise
			// process the document found from the collection and change the status from 'pending' to 'completed'
			if (found) {
				// call out the parameter that describes the result of the query when successful
				console.log(found); // document found from the db should be displayed in the terminal
				// modified the status of the returned document to 'completed'
				found.status = 'Completed';
				// save the new changes inside our database
				// upon saving the new changes for this document a 2nd promise will be instantiated
				// we will chain a thenable expression upon performing a save() method into our retured document
				return found.save().then((updatedTask, saveErr) => {
					// catch the state of the promise to identify a specific response
					if (updatedTask) {
						return 'Task has been successfully modified';
					} else {
						return 'Task failed to Update';
					}
				});
			} else {
				return 'Error! Match NOT Found for Task';
			}
		})
	};

	// 2. Change the status of task. completed -> pending
	module.exports.taskPending = (userInput) => {
		// expose this new component
		// search the database for the user input
		// findById mongoose method -> will run a search query inside our database using the id field as its reference
		// since performing this method will have 2 possible states/outcome, we will chain a then expression to handle the possible states of the promise
		// assign and invoke this new controller task
		return Task.findById(userInput).then((result, err) => {
			// handle and catch the state of the promise
			if (result) {
				// process the result of the query and extract the property to modify its value
				result.status = 'Pending';

				// save the new changes in the document
				return result.save().then((taskUpdated, error) => {
					// create a control structure to identify the proper response if the updates was successfully executed
					if (taskUpdated) {
						return `Task ${taskUpdated.name} was updated to Pending`;
					} else {
						return 'Error when saving task updates';
					}
				});
			} else {
				return 'Something Went Wrong';
			}
		});
	};

// [SECTION] - Destroy
	// 1. Remove an existing resource inside the TASK collection.
		// expose the data across other modules other app so that it will become reusable
		// would we need an input from the user? => which resource you want to target
	module.exports.deleteTask = (idNgTask) => {
		// how will the data will be processed in order to execute the task.
		// select which mongoose method will be used in order to acquire the desired end goal.
		// Mongoose -> it provides an interface and methods to be able to manipulate the resources found inside the mongoDB atlas.
		// in order to indetify the location in which the function will be executed append the model name

		// findByIdAndRemove => this is a mongoose method which targets a document using it's id field and removes the targeted document from the collection
		// upon executing this method within our collection a new promise will be instantiated upon waiting for the task to be executed, however we need to be able to handle the outcome of the promise whatever state in may fall on.
		// Promises in JS
			// => Pending (waiting to be executed)
			// => Fullfilled (successfully executed)
			// => Rejected (unfulfilled promise)
		// to be able to handle the possible states upon executing the task
		// we are going to insert a 'thenable' expression to determine HOW we will respond depending on the result of the promise
		// identify the 2 possible state of the promise using a then expression
		return Task.findByIdAndRemove(idNgTask).then((fulfilled, rejected) => {
			// identify how you will act according to the outcome
			if (fulfilled) {
				return 'The Task has been successfully Removed';
			} else {
				return 'Failed to remove Task';
			};
			// assign a new endpoint for this route
		});
	};


